package com.aniudhkashyap.spinnerdemo;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class SpinnerDemo extends ActionBarActivity {

    Spinner mSpinner;
    DeliveryOptionsAdapter mAdapter;

    String[] mDeliveryOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_demo);

        mDeliveryOptions =new String[]{getString(R.string.delivery_option_pickup),getString(R.string.delivery_option_normal),getString(R.string.delivery_option_express)};

        mSpinner=(Spinner)findViewById(R.id.spinner);
        mAdapter= new DeliveryOptionsAdapter(this,android.R.layout.simple_spinner_item, mDeliveryOptions);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinner.setAdapter(mAdapter);
    }


    class DeliveryOptionsAdapter extends ArrayAdapter<String> {

        public DeliveryOptionsAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            View dropDownItem = super.getDropDownView(position, convertView, parent);

            if (mDeliveryOptions[position].equals(getString(R.string.delivery_option_express)) && !isExpressDeliveryAvailableForArea(null)){
                dropDownItem.setClickable(true);
                dropDownItem.setEnabled(false);
            }else{
                dropDownItem.setClickable(false);
                dropDownItem.setEnabled(true);
            }
            return dropDownItem;
        }
    }

    /**
     * Returns the availability of express delivery for a given location
     * @param location Destination where express delivery is to be made
     * @return True: If delivery is available, else false.
     */
    private boolean isExpressDeliveryAvailableForArea(Location location){
        return false;
    }
}
